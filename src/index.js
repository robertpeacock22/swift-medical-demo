import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { combineReducers, createStore } from 'redux'
import { Provider } from 'react-redux'
import patientsReducer from './reducers/patientsReducer'
import woundsReducer from './reducers/woundsReducer'

import App from './App'

const rootReducer = combineReducers({ patientsReducer, woundsReducer })

const store = createStore(rootReducer)

ReactDOM.render(
  <Provider store={store}>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap' />
    <App />
  </Provider>,
  document.getElementById('root'),
)
