/*
 * Inspiration taken from https://medium.com/the-guild/injectable-services-in-react-de0136b6d476
 */

import React from 'react'
import PropTypes from 'prop-types'

const { createContext, useContext } = React

const ApiContext = createContext(null)

function patchWoundRequest(id) {
  return {
    data: {
      type: 'wounds',
      id: id.toString(),
      attributes: {
        resolved: true,
      },
    },
  }
}

function patchWoundHeaders(id) {
  return {
    headers: { 'Content-Type': 'application/json; charset=utf-8' },
    method: 'PATCH',
    body: JSON.stringify(patchWoundRequest(id)),
  }
}

const getPatient = (id) => fetch(`http://0.0.0.0:3000/patients/${id.toString()}`).then((response) => response.json())
const getPatients = () => fetch('http://0.0.0.0:3000/patients').then((response) => response.json())
const getWounds = (id) => fetch(`http://0.0.0.0:3000/patients/${id.toString()}/wounds`).then((response) => response.json())
const patchWound = (id) => fetch(`http://0.0.0.0:3000/wounds/${id}`, patchWoundHeaders(id)).then((response) => response.json())

export const ApiProvider = (props) => {
  const value = {
    getPatient: props.getPatient || getPatient,
    getPatients: props.getPatients || getPatients,
    getWounds: props.getWounds || getWounds,
    patchWound: props.patchWound || patchWound,
  }

  return (
    <ApiContext.Provider value={value}>
      {props.children}
    </ApiContext.Provider>
  )
}

export const useApi = () => useContext(ApiContext)

export const ApiConsumer = ApiContext.Consumer

ApiProvider.propTypes = {
  children: PropTypes.element.isRequired,
  getPatient: PropTypes.func,
  getPatients: PropTypes.func,
  getWounds: PropTypes.func,
  patchWound: PropTypes.func,
}
