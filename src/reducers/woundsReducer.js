const woundsReducer = (state = { wounds: {} }, action) => {
  switch (action.type) {
    case 'WOUNDS': {
      const wounds = action.wounds.length > 0
        ? action.wounds.reduce((acc, wound) => {
          const result = { ...acc }
          result[wound.id] = wound
          return result
        }, {})
        : {}

      return { wounds }
    }
    case 'WOUND': {
      const wounds = { ...state.wounds }
      wounds[action.wound.id] = action.wound

      return { ...state, wounds }
    }
    default:
      return state
  }
}

export default woundsReducer
