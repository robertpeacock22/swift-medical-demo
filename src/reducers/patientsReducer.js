
const patientsReducer = (state = { patients: {} }, action) => {
  switch (action.type) {
    case 'PATIENTS': {
      const patients = action.patients.length > 0
        ? action.patients.reduce((acc, patient) => {
          const result = { ...acc }
          result[patient.id] = patient
          return result
        }, {})
        : {}

      return { patients }
    }
    case 'PATIENT': {
      const patients = { ...state.patients }
      patients[action.patient.id] = action.patient

      return { ...state, patients }
    }
    default:
      return state
  }
}

export default patientsReducer
