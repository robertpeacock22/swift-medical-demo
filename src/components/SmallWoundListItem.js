import React from 'react'

import {
  makeStyles,
  Avatar,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@material-ui/core'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import { ResolveButton } from './ResolveButton'

const useStyles = makeStyles(() => ({
  row: {
    width: '100%',
    height: 'auto',
    overflow: 'hidden',
  },
  column: {
    width: '100%',
    display: 'block',
  },
  icon: {
    float: 'left',
    width: '20%',
    'min-width': '20%',
    'max-width': '20%',
  },
  name: {
    float: 'left',
    width: '80%',
    'min-width': '80%',
    'max-width': '80%',
  },
  status: {
    float: 'left',
    width: '33%',
    'min-width': '33%',
    'max-width': '33%',
  },
}))

export const SmallWoundListItem = ({ id }) => {
  const classes = useStyles()

  const {
    attributes:
      {
        imageUrl, inHouseAcquired, resolved, type,
      },
  } = useSelector((state) => state.woundsReducer.wounds)[id]

  return (
    <ListItem divider={true} key={id.toString()}>
      <div className={classes.column}>
        <div className={classes.row}>
          <ListItemAvatar className={classes.icon}>
            <Avatar alt={type} src={imageUrl} />
          </ListItemAvatar>
          <ListItemText className={classes.name} primary={type} />
        </div>
        <div className={classes.row}>
          <ListItemText className={classes.status} primary={'Origin'} secondary={inHouseAcquired ? 'In-House' : 'Pre-Existing'} />
          <ListItemText className={classes.status} primary={'Status'} secondary={resolved ? 'Resolved' : 'Unresolved'} />
          {(resolved === false) ? <ResolveButton className={classes.status} id={id} /> : null}
        </div>
      </div>
    </ListItem>
  )
}

SmallWoundListItem.propTypes = {
  id: PropTypes.number,
}

export default SmallWoundListItem
