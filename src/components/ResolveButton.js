import React from 'react'

import { Button } from '@material-ui/core'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'

import patchWound from '../actions/patchWound'
import { useApi } from '../api-service.jsx'

export const ResolveButton = ({ id }) => {
  const api = useApi()
  const dispatch = useDispatch()
  const [isSending, setIsSending] = React.useState(false)
  const sendRequest = React.useCallback(async () => {
    if (isSending) return

    setIsSending(true)
    await api.patchWound(id).then((results) => {
      dispatch(patchWound(results))
    })

    setIsSending(false)
  }, [api, dispatch, id, isSending])

  return (
    <Button type="button" disabled={isSending} onClick={sendRequest}>RESOLVE</Button>
  )
}

ResolveButton.propTypes = {
  id: PropTypes.number,
}

export default ResolveButton
