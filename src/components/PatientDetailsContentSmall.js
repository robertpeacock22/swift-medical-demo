import React from 'react'

import {
  makeStyles,
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Typography,
} from '@material-ui/core'

import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { useApi } from '../api-service.jsx'
import fetchPatient from '../actions/fetchPatient'
import { PatientDetailsContentFooter } from './PatientDetailsContentFooter'

const useStyles = makeStyles((theme) => ({
  content: {
    display: 'block',
    'text-align': 'left',
    width: '100%',
  },
  root: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  card: {
    width: '96%',
    margin: '2% 2% 2% 2%',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}))

export const PatientDetailsContentSmall = ({ id }) => {
  const classes = useStyles()
  const api = useApi()
  const dispatch = useDispatch()

  React.useEffect(() => {
    api.getPatient(id).then((results) => dispatch(fetchPatient(results.data)))
  }, [api, dispatch, id])

  const selectedPatient = useSelector((state) => state.patientsReducer.patients)[id]

  // TODO: Replace empty check with something nicer (lodash?)
  if (selectedPatient !== undefined && Object.entries(selectedPatient).length !== 0) {
    const { attributes } = selectedPatient

    const displayName = `${attributes.firstName} ${attributes.lastName}`

    return (
        <Card className={classes.card}>
          <CardHeader title={displayName} />
          <CardMedia
            className={classes.media}
            image={attributes.avatarUrl}
            title={`Picture of ${displayName}`} />
          <CardContent
            className={classes.content}>
            <Typography variant="body1" color="textPrimary" component="p">
              Date of Birth
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {attributes.dateOfBirth.substring(0, 10)}
            </Typography>
            <Typography variant="body1" color="textPrimary" component="p">
              Address
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {attributes.address}
            </Typography>
            <Typography variant="body1" color="textPrimary" component="p">
              Room #
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {attributes.roomNumber}
            </Typography>
            <Typography variant="body1" color="textPrimary" component="p">
              Bed #
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {attributes.bedNumber}
            </Typography>
          </CardContent>
          <PatientDetailsContentFooter />
        </Card>
    )
  }

  return <p>Loading {id}</p>
}

PatientDetailsContentSmall.propTypes = {
  id: PropTypes.string,
}

export default PatientDetailsContentSmall
