import React from 'react'

import {
  makeStyles,
  Box,
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Typography,
} from '@material-ui/core'

import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { useApi } from '../api-service.jsx'
import fetchPatient from '../actions/fetchPatient'
import { PatientDetailsContentFooter } from './PatientDetailsContentFooter'

const useStyles = makeStyles((theme) => ({
  content: {
    display: 'flex',
    margin: '0 5% 0 5%',
  },
  horizontal: {
    display: 'flex',
    width: '100%',
  },
  details: {
    'text-align': 'left',
    display: 'flex',
    flexDirection: 'column',
    width: '40%',
    margin: '0 5% 0 5%',
  },
  avatar: {
    width: 151,
    height: 151,
  },
  div: {
    display: 'flex',
    flexDirection: 'column',
  },
  card: {
    width: '80%',
    margin: '4% 10% 4% 10%',
  },
  face: {
    width: 151,
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}))

export const PatientDetailsContent = ({ id }) => {
  const classes = useStyles()
  const api = useApi()
  const dispatch = useDispatch()

  React.useEffect(() => {
    api.getPatient(id).then((results) => dispatch(fetchPatient(results.data)))
  }, [api, dispatch, id])

  const selectedPatient = useSelector((state) => state.patientsReducer.patients)[id]

  // TODO: Replace empty check with something nicer (lodash?)
  if (selectedPatient !== undefined && Object.entries(selectedPatient).length !== 0) {
    const { attributes } = selectedPatient

    const displayName = `${attributes.firstName} ${attributes.lastName}`

    return (
      <div>
        <Card className={classes.card}>
          <CardHeader title={displayName} />
          <div className={classes.content}>
            <CardMedia
                className={classes.avatar}
                image={attributes.avatarUrl}
                title={`Picture of ${displayName}`}
              />
              <CardContent
                className={classes.horizontal}>
                <Box className={classes.details}>
                  <Typography variant="body1" color="textPrimary" component="p">
                    Date of Birth
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    {attributes.dateOfBirth.substring(0, 10)}
                  </Typography>
                  <Typography variant="body1" color="textPrimary" component="p">
                    Address
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    {attributes.address}
                  </Typography>
                </Box>
                <Box className={classes.details}>
                  <Typography variant="body1" color="textPrimary" component="p">
                    Room #
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    {attributes.roomNumber}
                  </Typography>
                  <Typography variant="body1" color="textPrimary" component="p">
                    Bed #
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    {attributes.bedNumber}
                  </Typography>
                </Box>
              </CardContent>
            </div>
          <PatientDetailsContentFooter />
        </Card>
      </div>
    )
  }

  return <p>Loading {id}</p>
}

PatientDetailsContent.propTypes = {
  id: PropTypes.string,
}

export default PatientDetailsContent
