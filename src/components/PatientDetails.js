import React from 'react'
import PropTypes from 'prop-types'

import { useMediaQuery } from '@material-ui/core'

import { useParams } from 'react-router-dom'
import { ApiConsumer } from '../api-service.jsx'
import { PatientDetailsContent } from './PatientDetailsContent'
import { PatientDetailsContentSmall } from './PatientDetailsContentSmall'

export const PatientDetails = () => {
  const { id } = useParams()
  const useSmallLayout = !useMediaQuery('(min-width:600px)')

  return (
    <ApiConsumer>
      {
        () => (
          useSmallLayout
            ? (<PatientDetailsContentSmall id={id} />)
            : (<PatientDetailsContent id={id} />)

        )
      }
    </ApiConsumer>
  )
}

PatientDetails.propTypes = {
  location: PropTypes.any,
}

export default PatientDetails
