import React from 'react'

import {
  makeStyles,
  CardActions,
  Collapse,
  IconButton,
} from '@material-ui/core'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import clsx from 'clsx'

import { WoundList } from './WoundList'

const useStyles = makeStyles((theme) => ({
  content: {
    display: 'inline',
  },
  details: {
    float: 'left',
    width: '50%',
    'min-width': '50%',
    'max-width': '50%',
  },
  root: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  card: {
    width: '80%',
    margin: '10% 0 10% 0',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}))

// function footer(classes, expanded, handleExpandClick) {
export const PatientDetailsContentFooter = () => {
  const classes = useStyles()

  const [expanded, setExpanded] = React.useState(false)

  const handleExpandClick = () => {
    setExpanded(!expanded)
  }

  return (
    <div>
      <CardActions disableSpacing>
      <IconButton
        className={clsx(classes.expand, {
          [classes.expandOpen]: expanded,
        })}
        onClick={handleExpandClick}
        aria-expanded={expanded}
        aria-label="show more"
      >
        <ExpandMoreIcon />
      </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <WoundList />
      </Collapse>
    </div>
  )
}

export default PatientDetailsContentFooter
