import React from 'react'

import {
  makeStyles,
  useMediaQuery,
  Avatar,
  Card,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@material-ui/core'
import { useHistory } from 'react-router-dom'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import fetchPatients from '../actions/fetchPatients'
import { ApiConsumer, useApi } from '../api-service.jsx'

const useStyles = makeStyles(() => ({
  root: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  desktop: {
    width: '80%',
    margin: '4% 10% 4% 10%',
  },
}))

function listItemFromPatient({ attributes: { avatarUrl, firstName, lastName }, id }, history) {
  const displayName = `${firstName} ${lastName}`

  const patientDetails = () => {
    history.push(`/patient/${id}`)
  }

  return (
    <ListItem key={id.toString()} onClick={patientDetails}>
        <ListItemAvatar>
          <Avatar alt={displayName} src={avatarUrl} />
        </ListItemAvatar>
        <ListItemText primary={displayName} />
      </ListItem>
  )
}

function list(patients, history) {
  return (
    <ApiConsumer>
      {
        () => (
          <List>
            {
            patients.length > 0
              ? patients.map((patient) => listItemFromPatient(patient, history))
              : null
            }
          </List>
        )
      }
    </ApiConsumer>
  )
}

export const PatientList = () => {
  const history = useHistory()
  const api = useApi()
  const dispatch = useDispatch()
  const classes = useStyles()
  const useSmallLayout = !useMediaQuery('(min-width:600px)')

  React.useEffect(() => {
    api.getPatients({}).then((results) => {
      dispatch(fetchPatients(results.data))
    })
  }, [api, dispatch])

  const patientsObject = useSelector((state) => state.patientsReducer.patients)

  const patients = Object.values(patientsObject)

  if (useSmallLayout) {
    return (
      <Card>
        {list(patients, history)}
      </Card>
    )
  }

  return (
    <div className={classes.root}>
      <Card className={classes.desktop}>
        {list(patients, history)}
      </Card>
    </div>
  )
}

listItemFromPatient.propTypes = {
  attributes: {
    avatarUrl: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  },
  id: PropTypes.number,
  children: PropTypes.element.isRequired,
}

export default PatientList
