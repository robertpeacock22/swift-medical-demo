import React from 'react'

import {
  makeStyles,
  Avatar,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@material-ui/core'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import { ResolveButton } from './ResolveButton'

const useStyles = makeStyles(() => ({
  icon: {
    width: '10%',
    'min-width': '10%',
    'max-width': '10%',
  },
  name: {
    width: '30%',
    'min-width': '30%',
    'max-width': '30%',
  },
  status: {
    width: '20%',
    'min-width': '20%',
    'max-width': '20%',
  },
}))

export const WoundListItem = ({ id }) => {
  const classes = useStyles()

  const {
    attributes:
      {
        imageUrl, inHouseAcquired, resolved, type,
      },
  } = useSelector((state) => state.woundsReducer.wounds)[id]

  return (
    <ListItem key={id.toString()}>
        <ListItemAvatar className={classes.icon}>
          <Avatar alt={type} src={imageUrl} />
        </ListItemAvatar>
        <ListItemText className={classes.name} primary={type} />
        <ListItemText className={classes.status} primary={'Origin'} secondary={inHouseAcquired ? 'In-House' : 'Pre-Existing'} />
        <ListItemText className={classes.status} primary={'Status'} secondary={resolved ? 'Resolved' : 'Unresolved'} />
        {(resolved === false) ? <ResolveButton className={classes.status} id={id} /> : null}
    </ListItem>
  )
}

WoundListItem.propTypes = {
  id: PropTypes.number,
}

export default WoundListItem
