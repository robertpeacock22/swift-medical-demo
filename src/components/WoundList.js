import React from 'react'

import { useMediaQuery, List } from '@material-ui/core'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { ApiConsumer, useApi } from '../api-service.jsx'
import { WoundListItem } from './WoundListItem'
import { SmallWoundListItem } from './SmallWoundListItem'

import fetchWounds from '../actions/fetchWounds'

function smallListItemFromWoundId(id) {
  return <SmallWoundListItem key={id} id={id} />
}

function largeListItemFromWoundId(id) {
  return <WoundListItem key={id} id={id} />
}

function listItemFromWound(id, useSmallLayout) {
  if (useSmallLayout) {
    return smallListItemFromWoundId(id)
  }

  return largeListItemFromWoundId(id)
}

export const WoundList = () => {
  const useSmallLayout = !useMediaQuery('(min-width:600px)')
  const { id } = useParams()

  const api = useApi()
  const dispatch = useDispatch()

  React.useEffect(() => {
    api.getWounds(id).then((results) => {
      dispatch(fetchWounds(results.data))
    })
  }, [api, dispatch, id])

  const woundsObject = useSelector((state) => state.woundsReducer.wounds)

  const wounds = Object.values(woundsObject)

  if (wounds !== undefined) {
    return (
      <ApiConsumer>
        {
          () => (
            <List>
              {
                wounds.length > 0
                  ? wounds.map((wound) => listItemFromWound(wound.id, useSmallLayout))
                  : null
              }
            </List>
          )
        }
      </ApiConsumer>
    )
  }

  return (<div>Loading</div>)
}

export default WoundList
