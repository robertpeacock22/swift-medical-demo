const fetchPatients = (patientList) => (
  {
    type: 'PATIENTS',
    patients: patientList,
  }
)

export default fetchPatients
