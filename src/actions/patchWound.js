const patchWound = (wound) => (
  {
    type: 'WOUND',
    wound,
  }
)

export default patchWound
