const fetchWounds = (woundList) => (
  {
    type: 'WOUNDS',
    wounds: woundList,
  }
)

export default fetchWounds
