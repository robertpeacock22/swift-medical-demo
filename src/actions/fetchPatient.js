const fetchPatient = (patient) => (
  {
    type: 'PATIENT',
    patient,
  }
)

export default fetchPatient
