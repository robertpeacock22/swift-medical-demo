import React from 'react'
import PropTypes from 'prop-types'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'
import './App.css'
import { ApiProvider } from './api-service.jsx'
import { PatientList } from './components/PatientList'
import { PatientDetails } from './components/PatientDetails'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h3>Swift Medical</h3>
      </header>
      <div>
        <ApiProvider>
          <Router>
            <Switch>
              <Route exact path="/">
                <PatientList />
              </Route>
              <Route path="/patient/:id">
                <PatientDetails />
              </Route>
            </Switch>
          </Router>
        </ApiProvider>
      </div>
    </div>
  )
}

App.propTypes = {
  children: PropTypes.node,
}

export default App
