A take-home assignment for my interview with Swift Medical.

Before running this project, you need to pull and run the [Wounds Demo API](https://github.com/swiftmedical/wounds-demo-api).

Once you have the Wounds Demo API running, run this project with `npm start` and open [http://localhost:4000](http://localhost:4000) to view it in the browser.
